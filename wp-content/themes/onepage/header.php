<!doctype html>
<html>
	<head>
		<!-- Required meta tags -->
	    <meta charset="<?php bloginfo('charset'); ?>">
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	    <?php wp_head(); ?>
	</head>
	<body>

	<nav class="navbar navbar-expand-lg navbar-light bg-light">
  		<a class="navbar-brand" href="#"><?php bloginfo('name'); ?></a>

  		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    		<span class="navbar-toggler-icon"></span>
  		</button>
  		
  		<div class="collapse navbar-collapse" id="navbarNav">
		    <?php 
		    wp_nav_menu( 
		    	array( 
		    		'menu' => 'main_nav', /* menu name */
		    		'menu_class' => 'navbar-nav',
		    		'theme_location' => 'primary', /* where in the theme it's assigned */
		    		'container' => 'false', /* container class */
		    		// 'fallback_cb' => 'wp_bootstrap_main_nav_fallback', /* menu fallback */
		    		// 'depth' => '2',  suppress lower levels for now 
		    		'walker' => new WP_Bootstrap_Navwalker()
		    	)
		    );
		    ?>
  		</div>
</nav>



